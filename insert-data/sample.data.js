db.data.insertOne({ x: 1, y: "abc", text: "Hello, World!"});
db.data.insertOne({ x: 2, y: ["a", "b", "c"], z: { a: 10 }});
db.data.find({});
db.data.insertMany([
	{ x: 3, y: ["ccc", "bb", "a"], z: { a: 100 }},
	{ x: 4, y: ["xx", "yy", { text: "Lorem ipsum" }], z: { a: 1000 }},
	{ x: 1, y: ["xx"], z: { a: 1000 }}
]);
