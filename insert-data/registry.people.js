db.people.drop();

const peopleData = [{
	firstName: "Irene",
	lastName: "Martinelli",
	interests: ["sport", "math"],
	pets: [
		{ name: "Neve", type: "cat" }
	]
}, {
	firstName: "Laura",
	lastName: "Martinelli",
	interests: ["reading", "dolls"],
	pets: [
		{ name: "Neve", type: "cat" }
	]
}, {
	firstName: "Pietro",
	lastName: "Martinelli",
	interests: ["sport", "reading", "math", "science"],
	pets: [
		{ name: "Neve", type: "cat" },
		{ name: "Ruspa", type: "cat" },
		{ name: "Sofia", type: "cat" },
		{ name: "Ginevra", type: "bird" },
		{ name: "anonymous", type: "fish" }
	],
	data: ["pietrom", "martinellip"]
}, {
	firstName: "Cristina",
	lastName: "Russo",
	interests: ["technology", "fitness"],
	pets: [
		{ name: "Neve", type: "cat" }
	],
	data: 11
}, {
	firstName: "Maddalena",
	lastName: "Martinelli",
	interests: ["reading", "music"],
	pets: [
		{ name: "Ruspa", type: "cat" },
		{ name: "Sofia", type: "cat" },
		{ name: "Ginevra", type: "bird" },
		{ name: "anonymous", type: "hamster", color: "champagne" }
	]
}, {
	firstName: "Donald",
	lastName: "Duck",
	interests: ["sleeping"],
	pets: [
		{ name: "Malachia", type: "cat" }
	],
	tags: "comics",
	data: "donaldd"
}, {
	firstName: "Mickey",
	lastName: "Mouse",
	interests: ["investigations"],
	pets: [
		{ name: "Pluto", type: "dog" },
		{ name: "Figaro", type: "cat" }
	],
	tags: "comics",
	data: "mmouse"
}];


db.people.insertMany(peopleData);