db.getCollection("calendar.details").aggregate([
	{ $limit: 10 },
	{ $lookup: { 
		"from": "listings",
		"localField": "listing_id",
		"foreignField": "id",
		"as": "listing"
}},
	{ $project: { "listing_id": true, "listing": true } }

